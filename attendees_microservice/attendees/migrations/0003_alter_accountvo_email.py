# Generated by Django 4.0.3 on 2023-06-30 02:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0002_accountvo_rename_conferencev0_conferencevo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountvo',
            name='email',
            field=models.EmailField(max_length=254, null=True),
        ),
    ]

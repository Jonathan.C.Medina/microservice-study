import json
import pika
import django
import os
import sys
import time
from pika.exceptions import AMQPConnectionError
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    presentation = json.loads(body)
    send_mail(
        'Your presentation has been rejected!',
        f"{presentation['presenter_name']}, we're saddened to tell you that your presentation {presentation['title']} has been rejected",
        "admin@conference.go",
        [presentation['presenter_email']],
        fail_silently=False,
    )
    print("sent rejection notice")


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    presentation = json.loads(body)
    send_mail(
        'Your presentation has been approved!',
        f"{presentation['presenter_name']}, we're happy to tell you that your presentation {presentation['title']} has been accepted",
        "admin@conference.go",
        [presentation['presenter_email']],
        fail_silently=False,
    )
    print("sent approval notice")


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        print("Consuming has begun!")
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)


# def process_approval (ch, method, properties, body):
#     print("  Received %r" % body)
#     body = json.loads(body)
#     presenter_name = body["presenter_name"]
#     presenter_email = body["presenter_email"]
#     title = body["title"]
#     message_body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"
#     print(presenter_email)
#     send_mail(
#         'Your presentation has been approved!',
#         message_body,
#         "admin@conference.go",
#         [presenter_email],
#         fail_silently=False,
#     )
# def get_channel_and_connection(queue_name):
#     parameters = pika.ConnectionParameters(host='rabbitmq')
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue=queue_name)
#     # channel.basic_consume(
#     #     queue='tasks',
#     #     on_message_callback=process_approval,
#     #     auto_ack=True,
#     # )
#     # channel.start_consuming()
#     return channel, connection
